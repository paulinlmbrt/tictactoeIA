import sys


def ask_coords(grid):
    x, y = -1, -1
    coords_ok = False
    while not coords_ok:
        x = int(input("x? "))
        y = int(input("y? "))
        coords_ok = True if is_free(grid, x, y) else False
    return x, y


def is_free(grid, x, y):
    return grid[x][y] == ' '


def get_nb_plays(grid):
    i = 0
    for y in grid:
        for x in y:
            if x is not ' ':
                i += 1
    return i


def display_grid(grid):
    for y in grid:
        print("-------")
        sys.stdout.write('|')
        for x in y:
            sys.stdout.write(x + '|')
        print()
    print("-------")
