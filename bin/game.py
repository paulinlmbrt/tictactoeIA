from random import randint

from bin.interaction import display_grid


def pick_player(player1, player2):
    return player1 if randint(0, 1) == 0 else player2


class Game:
    H = 3
    W = 3

    def __init__(self, player1, player2):
        self.grid = [[' ' for x in range(self.W)] for y in range(self.H)]
        self.player1 = player1
        self.player2 = player2
        self.current_player = pick_player(self.player1, self.player2)
        self.won = False
        self.last_plays = []

    def start(self):
        while not self.won:
            self.swap_current_player()
            last_play = self.current_player.play(self)
            self.last_plays.append(last_play)
            display_grid(self.grid)
            print(self.grid)
            self.won = True if self.check_winner(last_play) else False
        print("Winner is " + self.current_player.name)

    def swap_current_player(self):
        self.current_player = self.player1 if self.current_player == self.player2 else self.player2

    def check_winner(self, coords):
        if self.grid[0][0] == self.grid[1][1] == self.grid[2][2] == self.current_player.char:
            return True
        if self.grid[2][0] == self.grid[1][1] == self.grid[2][0] == self.current_player.char:
            return True
        if self.grid[coords[0]][0] == self.grid[coords[0]][1] == self.grid[coords[0]][2] == self.current_player.char:
            return True
        if self.grid[0][coords[1]] == self.grid[1][coords[1]] == self.grid[2][coords[1]] == self.current_player.char:
            return True
        return False
