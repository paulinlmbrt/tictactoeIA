import random
from random import randint

from bin.interaction import is_free, get_nb_plays
from bin.player import Player


def edge_play(play):
    if play == (0, 1):
        return random.choice([(2, 0), (2, 2)])
    if play == (1, 0):
        return random.choice([(0, 2), (2, 2)])
    if play == (1, 2):
        return random.choice([(2, 0), (0, 0)])
    if play == (2, 1):
        return random.choice([(0, 2), (0, 0)])
    return None


def corner_play(play):
    if play == (0, 0):
        return 2, 2
    if play == (2, 2):
        return 0, 0
    if play == (0, 2):
        return 2, 0
    if play == (2, 0):
        return 0, 2
    return None


def is_center(play):
    return play == (1, 1)


def get_corner(grid):
    while True:
        corner = random.choice([(0, 2), (0, 0), (2, 0), (2, 2)])
        if is_free(grid, corner[0], corner[1]):
            return corner


class ComputerPlayer(Player):
    def play(self, game):
        coords = self.choose_intel_coords(game)
        game.grid[coords[1]][coords[0]] = self.char
        return coords

    def choose_rand_coords(self, game):
        x, y = -1, -1
        coords_ok = False
        while not coords_ok:
            x = randint(0, 2)
            y = randint(0, 2)
            coords_ok = True if is_free(game.grid, x, y) else False
        return x, y

    def choose_intel_coords(self, game):
        if get_nb_plays(game.grid) == 0:
            return 1, 1
        if get_nb_plays(game.grid) == 1:
            if is_center(game.last_plays[-1]):
                return get_corner(game.grid)
        if get_nb_plays(game.grid) == 2:
            to_play = edge_play(game.last_plays[-1])
            if to_play is not None:
                return to_play
            else:
                return corner_play(game.last_plays[-1])
        if get_nb_plays(game.grid) == 3:

