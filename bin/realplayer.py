from bin.interaction import ask_coords
from bin.player import Player


class RealPlayer(Player):
    def play(self, game):
        coords = ask_coords(game.grid)
        game.grid[coords[1]][coords[0]] = self.char
        return coords
