from bin.computerplayer import ComputerPlayer
from bin.game import Game
from bin.realplayer import RealPlayer

if __name__ == '__main__':
    player1 = RealPlayer("Paulin", 'X')
    player2 = ComputerPlayer("Computer", 'O')
    game = Game(player1, player2)
    game.start()
